{smcl}
{* *! version 1.0 28 Jul 2020 }{...}

{title:Title}

{phang}
{bf:gendum} {hline 2} Create and rename indicator variables for each value


{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:gendum}
{varname}, 
{cmdab:gen:erate(varname)}

{marker description}{...}
{title:Description}

{pstd}
{cmd:gendum} creates an indicator (dummy) variables for each value of a variable, 
then renames the result variables using those values.  

{pstd}
{cmd:gendum} is a wrapper for {cmd:tabulate} with the {it: generate} option, which produces
a series of variables named numerically.  {cmdab:gendum} does this, then renames the result 
variables to include the original values.  {p_end}

{marker example}{...}
{title:Example}

{pstd}
. sysuse auto{p_end}
{pstd}
. gendum make, gen(mkd) {p_end}

{marker Author}{...}
{title:Author}
{pstd}
Troy Payne {p_end}
{pstd}
Alaska Justice Information Center and University of Alaska Anchorage {p_end}
{pstd}
tpayne9@alaska.edu {p_end}

{pstd}
 {p_end}

