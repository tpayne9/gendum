*! 1.0 TCP 29 Jul 2020
* Generate well-named dummy variables
* Troy Payne
* January 7, 2020

version 16.0

cap prog drop gendum
prog define gendum

syntax varname , GENerate(name)
confirm new variable `generate'
cap ds `generate'*
	if !_rc {
		di as error "generate() must contain unique varname prefix"
		error 111
	}
	
tab `varlist', gen(`generate') missing

foreach v of varlist `generate'* {
	local vlabel: variable label `v'
	*local eqpos = strpos(`"`vlabel'"', "=") + 2
	*local vlabel = substr(`"`vlabel'"', strpos(`"`vlabel'"', "=") + 2, . )
	
	local vname =  strtoname("`generate'_" + (substr(`"`vlabel'"', strpos(`"`vlabel'"', "=") + 2, . )))
	
	if "`vname'" == "`generate'_" local vname = strtoname("`generate'_mis")
	
	else local vname: permname `vname', length(30)
		
		
	rename `v' `vname'
}
	





end
