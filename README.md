# gendum

A Stata command to create dummy variables from a categorical variable.  A wrapper for tabulate var , generate(newvar).

## Installation
~~~
    net from https://gitlab.com/tpayne9/gendum/-/raw/master/

    net install gendum
    
    help gendum
~~~

